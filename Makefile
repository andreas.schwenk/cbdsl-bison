# ------------------------------------------------------------------------------
# GNU Bison-Example by Andreas Schwenk, www.compiler-construction.com
# MIT License
# ------------------------------------------------------------------------------
# This Makefile creates the executable via $make
# ------------------------------------------------------------------------------

# ----- execuable name ---------------------------------------------------------

PROG = build/mydsl

# ----- configuration ----------------------------------------------------------

UNAME := $(shell uname)

CC = g++
FLEX = flex
ifeq ($(UNAME), Darwin) # macOS
	BISON = /usr/local/opt/bison/bin/bison
endif
ifeq ($(UNAME), Linux) # Linux
	BISON = /usr/bin/bison
endif

# ----- directories ------------------------------------------------------------

DIR_SRC = src/
DIR_TST = tests/
BUILD_DIR = build/

# ----- dependencies -----------------------------------------------------------

INC = -I/usr/local/lib/
LIB = -lm

HEADERS_1 += $(DIR_SRC)ast.hpp $(DIR_SRC)interpreter.hpp

HEADERS_2 = $(BUILD_DIR)mydsl.tab.hpp

HEADERS = $(HEADERS_1) $(HEADERS_2)
HEADERS_WO_PTABH = $(HEADERS_1)

OBJS =  $(BUILD_DIR)lex.yy.o $(BUILD_DIR)mydsl.tab.o
OBJS += $(BUILD_DIR)main.o $(BUILD_DIR)ast.o $(BUILD_DIR)interpreter.o

# ----- flags ------------------------------------------------------------------

CFLAGS =
# TODO: switch debug/release via configure
CFLAGS += -g3           # debug level 3
#CFLAGS += -O3           # optimize level 3

# ----- make build dir ---------------------------------------------------------

$(shell mkdir -p $(BUILD_DIR))

# ----- build execuable --------------------------------------------------------

$(PROG): $(OBJS)
	$(CC) $(OBJS) $(INC) $(LIB) -o $(PROG)

# ----- flex and bison ---------------------------------------------------------

$(BUILD_DIR)mydsl.tab.cpp $(BUILD_DIR)mydsl.tab.hpp: $(DIR_SRC)mydsl.ypp $(HEADERS_WO_PTABH)
	$(BISON) -v -d -o $(BUILD_DIR)mydsl.tab.cpp $(DIR_SRC)mydsl.ypp

$(BUILD_DIR)lex.yy.cpp: $(DIR_SRC)mydsl.lpp $(HEADERS)
	$(FLEX) -o $(BUILD_DIR)lex.yy.cpp $(DIR_SRC)mydsl.lpp

# ----- build object files -----------------------------------------------------

$(BUILD_DIR)main.o: $(DIR_SRC)main.cpp $(HEADERS)
	$(CC) $(DIR_SRC)main.cpp $(INC) $(CFLAGS) -c -o $(BUILD_DIR)main.o

$(BUILD_DIR)lex.yy.o: $(BUILD_DIR)lex.yy.cpp $(HEADERS)
	$(CC) $(BUILD_DIR)lex.yy.cpp $(INC) $(CFLAGS) -c -o $(BUILD_DIR)lex.yy.o

$(BUILD_DIR)mydsl.tab.o: $(BUILD_DIR)mydsl.tab.cpp $(HEADERS)
	$(CC) $(BUILD_DIR)mydsl.tab.cpp $(INC) $(CFLAGS) -c -o $(BUILD_DIR)mydsl.tab.o

$(BUILD_DIR)ast.o: $(DIR_SRC)ast.cpp $(HEADERS)
	$(CC) $(DIR_SRC)ast.cpp $(INC) $(CFLAGS) -c -o $(BUILD_DIR)ast.o

$(BUILD_DIR)interpreter.o: $(DIR_SRC)interpreter.cpp $(HEADERS)
	$(CC) $(DIR_SRC)interpreter.cpp $(INC) $(CFLAGS) -c -o $(BUILD_DIR)interpreter.o

# ----- clean ------------------------------------------------------------------

clean:
	rm -rf $(BUILD_DIR)
