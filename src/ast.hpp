/*
 ------------------------------------------------------------------------------
 GNU Bison-Example by Andreas Schwenk, www.compiler-construction.com
 MIT License
 ------------------------------------------------------------------------------
 This file implements the parser. The syntax of rules is decribed here:
 https://www.gnu.org/software/bison/manual/html_node/Rules-Syntax.html
 ------------------------------------------------------------------------------
*/

#ifndef __AST__HPP
#define __AST__HPP

#include <vector>
#include <string>

using namespace std;


enum ExpressionNodeType {
	ENT_Operator,
	ENT_Variable,
	ENT_ConstantInteger,
};
struct ExpressionNode { // for ease of use, all members are public
	enum ExpressionNodeType type;
	string string_value;
	int integer_value;
};


struct Expression { // for ease of use, all members are public
	vector<ExpressionNode*> postfix;

	Expression();
	~Expression();
};


class AST {  // abstract syntax tree

friend class Interpreter;

private:

	vector <Expression*> expressions;

	Expression *current_expression;

public:

	AST();
	~AST();

	void push_expression();

	void push_addition();
	void push_subtraction();
	void push_multiplication();
	void push_division();

	void push_identifier(string id);

	void push_constant_integer(int value);

};

#endif
