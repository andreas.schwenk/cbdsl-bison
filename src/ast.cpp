/*
 ------------------------------------------------------------------------------
 GNU Bison-Example by Andreas Schwenk, www.compiler-construction.com
 MIT License
 ------------------------------------------------------------------------------
 This file implements the parser. The syntax of rules is decribed here:
 https://www.gnu.org/software/bison/manual/html_node/Rules-Syntax.html
 ------------------------------------------------------------------------------
*/

#include <cassert>

#include "ast.hpp"

Expression::Expression() {
	this->postfix.clear();
}

Expression::~Expression() {
	// TODO: clean up!
}

AST::AST() {
	this->expressions.clear();
	this->current_expression = NULL;
}

AST::~AST() {
	// TODO: clean up!
}

void AST::push_expression() {
	Expression *expression = new Expression();
	this->expressions.push_back(expression);
	this->current_expression = expression;
}

void AST::push_addition() {
	assert(current_expression != NULL);
	ExpressionNode *node = new ExpressionNode();
	node->type = ENT_Operator;
	node->string_value = '+';
	this->current_expression->postfix.push_back(node);
}

void AST::push_subtraction() {
	assert(current_expression != NULL);
	ExpressionNode *node = new ExpressionNode();
	node->type = ENT_Operator;
	node->string_value = '-';
	this->current_expression->postfix.push_back(node);
}

void AST::push_multiplication() {
	assert(current_expression != NULL);
	ExpressionNode *node = new ExpressionNode();
	node->type = ENT_Operator;
	node->string_value = '*';
	this->current_expression->postfix.push_back(node);
}

void AST::push_division() {
	assert(current_expression != NULL);
	ExpressionNode *node = new ExpressionNode();
	node->type = ENT_Operator;
	node->string_value = '/';
	this->current_expression->postfix.push_back(node);
}

void AST::push_identifier(string id) {
	assert(current_expression != NULL);
	ExpressionNode *node = new ExpressionNode();
	node->type = ENT_Variable;
	node->string_value = string(id);
	this->current_expression->postfix.push_back(node);
}

void AST::push_constant_integer(int value) {
	assert(current_expression != NULL);
	ExpressionNode *node = new ExpressionNode();
	node->type = ENT_ConstantInteger;
	node->integer_value = value;
	this->current_expression->postfix.push_back(node);
}
