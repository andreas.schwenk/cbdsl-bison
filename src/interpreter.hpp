/*
 ------------------------------------------------------------------------------
 GNU Bison-Example by Andreas Schwenk, www.compiler-construction.com
 MIT License
 ------------------------------------------------------------------------------
 This file implements the interpreter.
 ------------------------------------------------------------------------------
*/

#ifndef __INTERPRETER__HPP
#define __INTERPRETER__HPP

#include "ast.hpp"

class Interpreter {

public:

	void interpret(AST *ast);

};

#endif
