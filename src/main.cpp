/*
 ------------------------------------------------------------------------------
 GNU Bison-Example by Andreas Schwenk, www.compiler-construction.com
 MIT License
 ------------------------------------------------------------------------------
 This file implements the parser. The syntax of rules is decribed here:
 https://www.gnu.org/software/bison/manual/html_node/Rules-Syntax.html
 ------------------------------------------------------------------------------
*/

#include <cassert>
#include <libgen.h>
#include <cstdio>
#include <cstdlib>
#include <unistd.h>

#include <iostream>
#include <string>

using namespace std;

#include "ast.hpp"
#include "interpreter.hpp"

extern int yylex();
extern int yyparse();

extern FILE *yyin; // input source file; must be a C-FILE (not a C++ stream)

AST *ast; // abstract syntax tree

string inputpath;
string inputdirname;
string inputbasename;

int main(int argc, char *argv[]) {

	if (argc != 2) {
		cerr << "Usage: mydsl inputfile" << endl;
		exit(EXIT_FAILURE);
	}

	inputpath = string(argv[1]);

	inputdirname = string(dirname(argv[1]));
	inputbasename = string(basename(argv[1]));

	// initialize
	ast = new AST();   // create the abstract syntax tree

	// parse mali source file
	yyin = fopen(inputpath.c_str(), "r");
	if (yyin == NULL) {
		cerr << "Error: invalid input path '" << inputpath << "'" << endl;
		exit(EXIT_FAILURE);
	}

	cout << "---------- TOKEN LIST ----------" << endl;

	yyparse();
	fclose(yyin);

	// interpret code
	Interpreter *interpreter = new Interpreter();
	interpreter->interpret(ast);

	// clean up
	delete ast;
	delete interpreter;


	exit(EXIT_SUCCESS);
}
