/*
 ------------------------------------------------------------------------------
 GNU Bison-Example by Andreas Schwenk, www.compiler-construction.com
 MIT License
 ------------------------------------------------------------------------------
 This file implements the interpreter.
 ------------------------------------------------------------------------------
*/

#include <cassert>

#include <iostream>

using namespace std;

#include "interpreter.hpp"

void Interpreter::interpret(AST *ast) {

	const int n1 = ast->expressions.size();

	for(int i=0; i<n1; i++) {
		Expression *exp = ast->expressions[i];

		vector <int> stack;
		string op;
		int operand0;
		int operand1;

		const int n2 = exp->postfix.size();
		for(int j=0; j<n2; j++) {
			ExpressionNode *node = exp->postfix[j];
			switch(node->type) {
			case ENT_ConstantInteger:
				stack.push_back(node->integer_value);
				break;
			case ENT_Operator:
				operand1 = stack.back();
				stack.pop_back();
				operand0 = stack.back();
				stack.pop_back();
				op = node->string_value;   // operator
				if(op.compare("+") == 0)
					stack.push_back(operand0 + operand1);
				else if(op.compare("-") == 0)
					stack.push_back(operand0 - operand1);
				else if(op.compare("*") == 0)
					stack.push_back(operand0 * operand1);
				else if(op.compare("/") == 0)
					stack.push_back(operand0 / operand1);
				else
					assert(false); // unimplemented
				break;
			case ENT_Variable:
				if(node->string_value.compare("eight") == 0)
					stack.push_back(8);
				else {
					cout << "Interpretation-Error: unknown variable " << node->string_value << endl;
				}
				break;
			}

		}

		if(stack.size() != 1)
			cout << "Interpretation-Error: stack size is incorrect" << endl;

		cout << "Evaluation of expression " << i << ": " << stack[0] << endl;

	}

}
