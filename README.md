# GNU Bison Example

GNU Bison-Example (C/C++) for my lecture CBDSL. Refer to: www.compiler-construction.com

The code focusses on parsing and interpretation.

*I am thankful to any kind of feedback. Please send a mail to [errata@compiler-construction.com](mailto:errata@compiler-construction.com).*

## Installation (here: Linux)

`sudo apt-get install g++ flex bison`

## Run this Project

Compile: *The executable is written into the `build/` directory*

`make`

Run:

`build/mydsl mydslcode.txt`

## Description of the Files provided

*  `main.cpp`: main function
*  `ast.cpp`, `ast.hpp`: abstract syntax tree
*  `interpreter.cpp`, `interpreter.hpp`: interpreter
*  `mydsl.lpp`: lexical analyzer (lexer)
*  `mydsl.ypp`: parser
*  `mydslcode.txt`: Example code written in the DSL
*  `Makefile`: script to build the project via `make`

## External Documentation

[https://www.gnu.org/software/bison/manual/](https://www.gnu.org/software/bison/manual/)

## Example Run

Content of `mydslcode.txt`:

```
1 + 2;
(1+2+3) * (3+4);
2 * eight;
// this is a comment line
```

Interpreted Output:

```
3
42
16
```

## Renaming

You can rename the language name e.g. from `mydsl` to `calculator` as follows:

```
OLD_NAME=mydsl
NEW_NAME=calculator
sed -i .bak "s/${OLD_NAME}/${NEW_NAME}/g" Makefile src/${OLD_NAME}.lpp src/${OLD_NAME}.ypp
rm Makefile.bak
rm src/${OLD_NAME}.lpp.bak
rm src/${OLD_NAME}.ypp.bak
mv src/${OLD_NAME}.lpp src/${NEW_NAME}.lpp
mv src/${OLD_NAME}.ypp src/${NEW_NAME}.ypp
```
